﻿#include <iostream>
#include "Helpers.h"
#include <string>

class Animal
{
public:
	virtual void Voice() = 0
	{
	}
};

class cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "SHAZOO!\n";
	}
};

class pig : public Animal
{
public:
	void Voice() override
	{
		std::cout << "HONK!\n";
	}
};

class elephant : public Animal
{
public:
	void Voice() override
	{
		std::cout << "WAMP!\n";
	}
};

int main()
{
	cow* x = new cow();
	pig* y = new pig();
	elephant* z = new elephant();
	Animal* ar[]{x, y, z};
	
	for (int i = 0; i < 3; ++i)
	{
		ar[i]->Voice();
	}
	delete x, y, z;
}